import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import com.google.gson.*;

public class WxHw extends Application
{
  TextField txtenter;
  Button btncreate;
  Label display;
  String wxReport;

  @Override
  public void start(Stage primaryStage) {
    // Make the controls
    txtenter=new TextField("Enter a zip code");
    display=new Label("Weather Information");
    btncreate=new Button("Display weather");
    // Center text in label
    txtenter.setAlignment(Pos.CENTER);
    display.setAlignment(Pos.CENTER);
    btncreate.setAlignment(Pos.CENTER);
    // Apply ccs-like style to label
    display.setStyle("-fx-border-color: #000; -fx-padding: 5px;");


    // Make container for app
    VBox root = new VBox();
    // Put container in middle of scene
    root.setAlignment(Pos.CENTER);
    // Spacing between items
    root.setSpacing(15);
    // Add to VBox
    root.getChildren().add(txtenter);
    root.getChildren().add(btncreate);
    root.getChildren().add(display);
    // Set widths
    setWidths();
    //attach buttons to code in separate method
    attachCode();
    // Set the scene
    Scene scene = new Scene(root, 630, 630);
    primaryStage.setTitle("Dakota Garner Conn");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public void setWidths(){
    // Set widths of all controls
    txtenter.setMaxWidth(800);
    btncreate.setPrefWidth(400);
    display.setPrefWidth(800);
    display.setPrefHeight(500);
  }

  public void attachCode()
  {
    // Attach actions to buttons
    btncreate.setOnAction(e -> btncreatecode(e));
  }
  
  public void btncreatecode(ActionEvent e)
  {
      display.setText(getURL(txtenter.getText()));
  }


   
   public String getURL(String zipCode)
	{
		JsonElement jse = null;
      String newURL = null;

		try
		{
			// Construct WxStation API URL
			URL bitlyURL = new URL("http://api.wunderground.com/api/716973fa0f3aaf1a/geolookup/conditions/q/" + zipCode + ".json");

			// Open the URL
			InputStream is = bitlyURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
      // Build a weather report
      String city = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("display_location")
      .getAsJsonObject().get("full").getAsString();
      wxReport = "Location: " + city + "\n";

      String time = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("observation_time").getAsString();
      wxReport = wxReport + "Time: " + time + "\n";
      
      String weather = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("weather").getAsString();
      wxReport = wxReport + "Weather: " + weather + "\n";

      String temp = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("temp_f").getAsString();
      wxReport = wxReport + "Temperature F: " + temp + "\n";
      
      String wind = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("wind_string").getAsString();
      wxReport = wxReport + "Wind: " + time + "\n";

      String pressure = jse.getAsJsonObject().get("object")
      .getAsJsonObject().get("current_observation")
      .getAsJsonObject().get("preasure_in").getAsString();
      wxReport = wxReport + "Pressure in HG: " + pressure + "\n";

		}
    return wxReport;
	}

	public static void main(String[] args)
	{
		WxHw b = new WxHw();
    if ( args.length == 0 )
      System.out.println("Please enter a zip code as the first argument.");
    else
    {
		  String url = b.getURL(args[0]);
      if ( url != null )
		    System.out.println(url);
    }
    launch(args);
	}
}
